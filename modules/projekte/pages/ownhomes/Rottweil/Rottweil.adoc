# Projekt Raum Rottweil

[#tab-18068d4d-9ee8-4349-9a42-bba5b22972ac]
.Projekt Visitenkarte
|===
|Ort  |Raum Rottweil
|Grundriss  |8 x 5 Meter, zweigeschossig
|Status |Bauantrag genehmigt
|Baubeginn |Frühjahr  2020
|Pflanzenkläranlage |Ja
|Stromanschluß |Nein
|Wasseranschluß |Nein
|===

## Ansichten
[#fig-c00b210c-d571-4019-a2a3-62da76f79236]
.Grundriss
image::ownhomes/Rottweil/Grundriss.jpeg[Grundriss]

[#fig-bd22f328-92a0-44c4-9beb-fbf444b2203a]
.Norden
image::ownhomes/Rottweil/Norden.jpeg[Norden]

[#fig-cfa1f214-bec1-49c8-93a4-f66d3ccdcc85]
.Osten
image::ownhomes/Rottweil/Osten.jpeg[Osten]

[#fig-7e0b5e44-7611-411b-a268-83255c737570]
.Westen
image::ownhomes/Rottweil/Westen.jpeg[Westen]

[#fig-420d310f-5792-4924-a6d7-0547d2e0ac31]
.Süden
image::ownhomes/Rottweil/Süden.jpeg[Süden]

[#fig-b141969e-98b2-4151-81c2-a37bf682e16a]
.Süden innen
image::ownhomes/Rottweil/Süden_Innen.jpeg[Süden innen]

